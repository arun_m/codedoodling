/**
 * 
 */
package edu.arun.codedoodle.stringsymmetry;

/**
 * @author amukherjee
 *
 */
public class CodeSymmetry {

	private char _openChar;
	private char _closeChar;
	
	public CodeSymmetry(char openChar, char closeChar){
		_openChar = openChar;
		_closeChar = closeChar;
	}
	
	public boolean isStringSymmetricUsingCounter(String input){
		int counter = 0; 
		
		for(char in : input.toCharArray())
		{
			if(in == _openChar){
				counter += 1;
			} else if(in == _closeChar){
				counter -= 1;
			}
		}
		
		return counter == 0 ? true : false;
	}
	
	public boolean isStringSymmetricUsingLogic(String input){
		char charHolder = '\0'; 
		
		for(char in : input.toCharArray())
		{
			charHolder ^= in;
		}
		
		return charHolder == '\0' ? true : false;
	}	
}
