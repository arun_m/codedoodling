package edu.arun.codedoodle.stringsymmetry;

import static org.junit.Assert.*;

import org.junit.Test;

public class CodeSymmetryTest {
	
	private CodeSymmetry _target = new CodeSymmetry('(', ')');

	@Test
	public final void should_return_true_with_counter() {
		// Arrange
		String mockVal = "(())";
		
		// Act
		boolean result = _target.isStringSymmetricUsingCounter(mockVal);
		
		// Assert
		assertTrue(result);
	}
	
	@Test
	public final void should_return_false_with_counter() {
		// Arrange
		String mockVal = "(()";
		
		// Act
		boolean result = _target.isStringSymmetricUsingCounter(mockVal);
		
		// Assert
		assertFalse(result);
	}	
	

	@Test
	public final void should_return_true_with_logic() {
		// Arrange
		String mockVal = "(())";
		
		// Act
		boolean result = _target.isStringSymmetricUsingLogic(mockVal);
		
		// Assert
		assertTrue(result);
	}
	
	@Test
	public final void should_return_false_with_logic() {
		// Arrange
		String mockVal = "(()";
		
		// Act
		boolean result = _target.isStringSymmetricUsingLogic(mockVal);
		
		// Assert
		assertFalse(result);
	}

}
